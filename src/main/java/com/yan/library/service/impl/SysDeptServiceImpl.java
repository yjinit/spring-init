package com.yan.library.service.impl;

import com.yan.common.core.domain.entity.SysDept;
import com.yan.library.mapper.SysDeptMapper;
import com.yan.library.service.ISysDeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 部门表(SysDept)表服务实现类
 * @author yanjun
 * @since 2024-01-15 09:30:12
 */
@Slf4j
@Service
public class SysDeptServiceImpl implements ISysDeptService {
    @Resource
    private SysDeptMapper sysDeptMapper;

    /**
     * 按主键查询单条数据
     * @param deptId 主键
     * @return 实例对象
     */
    @Override
    public SysDept selectByDeptId(Long deptId) {
        return sysDeptMapper.selectByDeptId(deptId);
    }


    /**
     * 按条件查询列表
     * @param sysDept 查询条件
     * @return 对象列表
     */
    @Override
    public List<SysDept> selectSysDeptList(SysDept sysDept) {
        return sysDeptMapper.selectSysDeptList(sysDept);
    }

    /**
     * 新增数据
     * @param sysDept 实例对象
     * @return 影响行数
     */
    @Override
    public int insert(SysDept sysDept) {
        return sysDeptMapper.insert(sysDept);
    }

    /**
     * 批量新增或按主键更新数据
     * @param sysDepts 实例对象列表
     * @return 影响行数
     */
    @Override
    public int insertOrUpdateBatch(List<SysDept> sysDepts) {
        return sysDeptMapper.insertOrUpdateBatch(sysDepts);
    }


    /**
     * 批量新增数据
     * @param sysDepts 实例对象列表
     * @return 影响行数
     */
    @Override
    public int insertBatch(List<SysDept> sysDepts) {
        return sysDeptMapper.insertBatch(sysDepts);
    }

    /**
     * 修改数据
     * @param sysDept 实例对象
     * @return 影响行数
     */
    @Override
    public int update(SysDept sysDept) {
        return sysDeptMapper.update(sysDept);
    }

    /**
     * 按主键删除数据
     * @param deptId 主键
     * @return 影响行数
     */
    @Override
    public int deleteById(Long deptId) {
        return sysDeptMapper.deleteById(deptId);
    }

    /**
     * 批量主键删除数据
     * @param deptIds 主键数组
     * @return 影响行数
     */
    @Override
    public int deleteByDeptIds(Long[] deptIds) {
        return sysDeptMapper.deleteByDeptIds(deptIds);
    }

}
