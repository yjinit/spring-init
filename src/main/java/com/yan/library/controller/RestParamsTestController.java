package com.yan.library.controller;

import com.alibaba.fastjson2.JSON;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/test")
@Slf4j
public class RestParamsTestController {

    //请求格式1：curl -X GET "http://localhost:8080/api/test/list?ids=1,2,3"
    //请求格式2：curl -X GET "http://localhost:8080/api/test/list?ids=1&ids=2&ids=3"
    @GetMapping("/list")
    public String getByList(@RequestParam(required = true) List<String> ids) {
        String paramJson = JSON.toJSONString(ids);
        log.info("getByList: {}", paramJson);
        return paramJson;
    }

    //请求格式1：curl -X POST "http://localhost:8080/api/test/list" -H "Content-Type: application/x-www-form-urlencoded" --data-urlencode "ids=1,2,3"
    //请求格式2：curl -X POST "http://localhost:8080/api/test/list" -H "Content-Type: application/x-www-form-urlencoded" --data-urlencode "ids=1" --data-urlencode "ids=2" --data-urlencode "ids=3"
    @PostMapping("/list")
    public String postByList(ParamListDto dto) {
        String paramJson = JSON.toJSONString(dto);
        log.info("postByList: {}", paramJson);
        return paramJson;
    }

    //请求格式：curl -X DELETE "http://localhost:8080/api/test/list/1,2,3"
    @DeleteMapping("/list/{ids}")
    public String deleteByList(@PathVariable(required = true) List<String> ids) {
        String paramJson = JSON.toJSONString(ids);
        log.info("deleteByList: {}", paramJson);
        return paramJson;
    }

    //请求格式1：curl -X GET "http://localhost:8080/api/v1/tests/array?ids=1,2,3"
    //请求格式2：curl -X GET "http://localhost:8080/api/v1/tests/array?ids=1&ids=2&ids=3"
    @GetMapping("/array")
    public String getByArray(@RequestParam(required = true) String[] ids) {
        String paramJson = JSON.toJSONString(ids);
        log.info("getByArray: {}", paramJson);
        return paramJson;
    }

    //请求格式1：curl -X POST "http://localhost:8080/api/v1/tests/array" -H "Content-Type: application/x-www-form-urlencoded" --data-urlencode "ids=1,2,3"
    //请求格式2：curl -X POST "http://localhost:8080/api/v1/tests/array" -H "Content-Type: application/x-www-form-urlencoded" --data-urlencode "ids=1" --data-urlencode "ids=2" --data-urlencode "ids=3"
    @PostMapping("/array")
    public String postByArray(ParamArrayDto dto) {
        String paramJson = JSON.toJSONString(dto);
        log.info("postByArray: {}", paramJson);
        return paramJson;
    }

    //请求格式：curl -X DELETE "http://localhost:8080/api/v1/tests/array/1,2,3"
    @DeleteMapping("/array/{ids}")
    public String deleteByArray(@PathVariable(required = true) String[] ids) {
        String paramJson = JSON.toJSONString(ids);
        log.info("deleteByArray: {}", paramJson);
        return paramJson;
    }

    //请求格式：curl -X POST "http://localhost:8080/api/test/addPost" -H "Content-Type: application/x-www-form-urlencoded" -d "name='张三'&age=19"
    //请求格式：curl -X POST "http://localhost:8080/api/test/addPost" -H "Content-Type: multipart/form-data" --form "name='张三'" --form "age=19"
    @PostMapping("/addPost")
    public String addPost(Demo demo) {
        String paramJson = JSON.toJSONString(demo);
        log.info("demo:{}", paramJson);

        return paramJson;
    }

    //请求格式：curl -X POST "http://localhost:8080/api/test/addPostJson" -H "Content-Type: application/json" -d "{\"name\": \"张三\", \"age\": 19}"
    @PostMapping("/addPostJson")
    public String addPostJson(@RequestBody Demo demo) {
        String paramJson = JSON.toJSONString(demo);
        log.info("demo:{}", paramJson);

        return paramJson;
    }

    //请求格式：curl -X GET "http://localhost:8080/api/test/addGet?name='yanjun'&age=10"
    @GetMapping("/addGet")
    public String addGet(Demo demo) {
        String paramJson = JSON.toJSONString(demo);
        log.info("demo:{}", paramJson);

        return paramJson;
    }

    // curl -X POST "http://localhost:8080/api/test/addMap" -H "Content-Type: application/json" -d "{\"name\": \"张三\", \"age\": 19}"
    @RequestMapping(value = "/addMap", method = RequestMethod.POST)
    public String addMap(@RequestBody Map<String, String> map) throws Exception {
        String paramJson = JSON.toJSONString(map);
        System.out.println(paramJson);
        return paramJson;
    }

    // curl -X POST "http://localhost:8080/api/test/listMap" -H "Content-Type: application/json" -d "[{\"name\":\"li\",\"age\":18},{\"name\":\"zhang\",\"age\":100}]
    @RequestMapping(value = "/listMap", method = RequestMethod.POST)
    public String listMap(@RequestBody List<Map<String, String>> maps) throws Exception {
        String paramJson = JSON.toJSONString(maps);
        System.out.println(paramJson);
        return paramJson;
    }

    //请求格式：curl -X POST "http://localhost:8080/api/test/addList" -H "Content-Type: application/json" -d "[{\"name\":\"li\",\"age\":18},{\"name\":\"zhang\",\"age\":100}]
    @PostMapping("/addList")
    public String add(@RequestBody List<Demo> list) {
        String paramJson = JSON.toJSONString(list);
        System.out.println(paramJson);
        return paramJson;
    }

}

@Data
class ParamListDto {
    private List<String> ids;

}

@Data
class ParamArrayDto {
    private String[] ids;

}

@Data
class Demo {
    private String name;
    private Integer age;
}

