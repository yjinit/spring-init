package com.yan.library.controller;

import com.yan.common.core.domain.entity.SysMenu;
import com.yan.common.core.domain.entity.SysUser;
import com.yan.library.service.ISysMenuService;
import com.yan.library.service.ISysUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class IndexController {

    @Resource
    private ISysMenuService menuService;

    @Resource
    private ISysUserService userService;

    @GetMapping("/index")
    public String index(ModelMap modelMap){
        SysUser user = userService.selectUserByLoginName("admin");
        List<SysMenu> menus = menuService.selectMenusByUser(user);
        modelMap.put("user",user);
        modelMap.put("menus",menus);
        modelMap.put("footer", false);
        modelMap.put("tagsView", true);
        Boolean footer = false;
        Boolean tagsView = true;
        modelMap.put("mainClass", contentMainClass(footer, tagsView));
        return "index";
    }

    // 切换主题
    @GetMapping("/switchSkin")
    public String switchSkin()
    {
        return "skin";
    }

    public String contentMainClass(Boolean footer, Boolean tagsView)
    {
        if (!footer && !tagsView)
        {
            return "tagsview-footer-hide";
        }
        else if (!footer)
        {
            return "footer-hide";
        }
        else if (!tagsView)
        {
            return "tagsview-hide";
        }
        return StringUtils.EMPTY;
    }
}
