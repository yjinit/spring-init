package com.yan.library.controller;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
public class JdbcController {
    @Resource
    JdbcTemplate jdbcTemplate;

    @RequestMapping("selectAll")
    public List<Map<String, Object>> selectAll() {
        String sql = "select * from books";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql);
        return maps;
    }

}
