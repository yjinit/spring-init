package com.yan.library.controller;

import com.yan.common.core.controller.BaseController;
import com.yan.common.core.domain.AjaxResult;
import com.yan.common.core.domain.entity.SysDept;
import com.yan.common.core.page.TableDataInfo;
import com.yan.library.service.ISysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 部门表控制层
 *
 * @author yanjun
 * @since 2024-01-15 09:35:05
 */
@Controller
@RequestMapping("/sysDept")
public class SysDeptController extends BaseController{
    /**
     * 服务对象
     */
    @Autowired
    private ISysDeptService sysDeptService;

    @GetMapping()
    public String dept(){
        return "library/list";
    }

    /**
     * 查询部门表列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SysDept sysDept){
        startPage();
        List<SysDept> list = sysDeptService.selectSysDeptList(sysDept);
        return getDataTable(list);
    }


    /**
     * 查询部门表详细信息
     */
    @GetMapping(value = "/{deptId}")
    public AjaxResult getInfo(@PathVariable("deptId") Long deptId)
    {
        return success(sysDeptService.selectByDeptId(deptId));
    }

    /**
     * 新增部门表
     */
    @PostMapping
    public AjaxResult add(@RequestBody SysDept sysDept)
    {
        return toAjax(sysDeptService.insert(sysDept));
    }

    /**
     * 修改部门表
     */
    @PutMapping
    public AjaxResult edit(@RequestBody SysDept sysDept)
    {
        return toAjax(sysDeptService.update(sysDept));
    }  

    /**
     * 删除部门表
     */
    @DeleteMapping("/{deptIds}")
    public AjaxResult remove(@PathVariable Long[] deptIds)
    {
        return toAjax(sysDeptService.deleteByDeptIds(deptIds));
    }
    
}
