package com.yan.library.mapper;

import com.yan.common.core.domain.entity.SysDept;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 部门表(SysDept)表数据库访问层
 * @author yanjun
 * @since 2024-01-15 09:30:05
 */
public interface SysDeptMapper {

    /**
     * 按主键查询单条数据
     * @param deptId 主键
     * @return 实例对象
     */
    SysDept selectByDeptId(Long deptId);

    /**
     * 按条件查询列表
     * @param sysDept 查询条件
     * @return 对象列表
     */
    List<SysDept> selectSysDeptList(SysDept sysDept);

    /**
     * 计算行数
     * @param sysDept 查询条件
     * @return 总行数
     */
    long count(SysDept sysDept);

    /**
     * 新增数据
     * @param sysDept 实例对象
     * @return 影响行数
     */
    int insert(SysDept sysDept);

    /**
     * 批量新增数据
     * @param entities List<SysDept> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<SysDept> entities);

    /**
     * 批量新增或按主键更新数据
     * @param entities List<SysDept> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<SysDept> entities);

    /**
     * 修改数据
     * @param sysDept 实例对象
     * @return 影响行数
     */
    int update(SysDept sysDept);

    /**
     * 按主键删除数据
     * @param deptId 主键
     * @return 影响行数
     */
    int deleteById(Long deptId);

    /**
     * 批量主键删除数据
     * @param deptIds 主键数组
     * @return 影响行数
     */
    int deleteByDeptIds(Long[] deptIds);

}
